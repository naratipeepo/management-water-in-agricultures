import jsonwebtoken from "jsonwebtoken";
import { jwtSecret } from "../config/jwtConfig.js";
import bcrypt from "bcrypt";
import User from "../model/userModel.js";
import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;
import Garden from "../model/gardenModel.js";

var ObjectId = mongoose.Types.ObjectId;

export function checkLogon(req, res) {
  const { email, password } = req.body;
  if (!email || !password)
    res.status(404).json({ errors: { global: "Invalid credential" } });
  User.findOne({ email: email })
    .catch((err) => res.status(400).json({ errors: { global: err } }))
    .then((user) => {
      if (!user) {
        return res.status(400);
      }
      bcrypt
        .compare(password, user.password)
        .then((matched) => {
          console.log(matched);
          if (matched) {
            const token = jsonwebtoken.sign(
              { email: user.email },
              jwtSecret,
              { expiresIn: 3600 } // 1 hour
            );
            res.json({
              user: {
                pid: user.pid,
                role: user.role,
                expiresIn: 3600
              },
              success: true,
              token: "Bearer " + token,
            });
          } else
            res.status(404).json({ errors: { global: "Invalid password" } });
        })
        .catch((err) => {
          res.status(404).json({ errors: { global: "Invalid credential!" } });
        });
    });
}

export const getUserByPID = (req, res) => {
  User.findOne({ pid: req.params.pid })
    .populate({
      path: 'garden',
      // Get friends of friends - populate the 'friends' array for every friend
      populate: { path: 'plants' }
    })
    .then((user) => {
      if (!user) {
        res.status(404)
      }
      res.json(user);
    })
    .catch((err) => res.status(404));
};

export const createUser = (req, res) => {
  let { name, surname, pid, password, address, email } = req.body;

  //hashing-password
  bcrypt.hash(password, 10).then((hashingPassword) => {
    var newUser = new User({
      name: name,
      surname: surname,
      pid: pid,
      password: hashingPassword,
      address: address,
      email: email,
    });
    User.init() // avoid dup by wait until finish building index
      .then(function () {
        newUser
          .save()
          .then((user) => {
            res.json({ success: true, message: "User createUser" });
          })
          .catch((err) => {
            res.status(400);
          });
      });
  });
};

export const updateUser = (req, res) => {
  let { name, surname, pid, password, address, email } = req.body;
  //hashing-password
  bcrypt.hash(password, 10).then((hashingPassword) => {
    var data = {
      name: name,
      surname: surname,
      pid: pid,
      password: hashingPassword,
      address: address,
      email: email,
    }
    User.findOneAndUpdate({ pid: pid }, data)
      .then((user) => {
        user
          .save()
          .then((u) => res.json({ success: true, message: u }))
          .catch((err) => {
            res.status(400).json(err);
          });
      })
      .catch((err) => {
        res.status(400).json(err);
      });
  })
    .catch((e) => {

      var data = {
        name: name,
        surname: surname,
        pid: pid,
        address: address,
        email: email,
      }
      User.findOneAndUpdate({ pid: pid }, data)
        .then((user) => {
          user
            .save()
            .then((u) => res.json({ success: true, message: u }))
            .catch((err) => {
              res.status(400).json(err);
            });
        })
        .catch((err) => {
          res.status(400).json(err);
        });

    });
};

export const deleteUser = (req, res) => {
  User.findByIdAndRemove({ _id: new ObjectId(req.params.id) })
    .then((user) => {
      if (!user) {
        res
          .status(404)
          .json(logError("User not found with id " + req.params.userId));
      }
      res.json({ success: true, message: "User deleted successfully!" });
    })
    .catch((err) => res.status(404));
};

export const addGarden = (req, res) => {
  let { color, point, pid, soilHumidity, temperature, plant } = req.body;
  var newGarden = new Garden({
    color: color,
    point: point,
    soilHumidity: soilHumidity,
    temperature: temperature,
    plants: plant
  });

  Garden.init() // avoid dup by wait until finish building index
    .then(function () {
      newGarden
        .save()
        .then((garden) => {
          User.findOne({ pid: pid })
            .then((user) => {
              if (!user) {
                res.status(404)
              }
              user.garden.push(garden._id);
              user
                .save()
                .then((user) => {
                  res.json({ success: true, message: "add success" });
                })
                .catch((err) => {
                  console.log(err);
                  res.status(400);
                });
            })
            .catch((err) => {
              console.log(err);
              res.status(404);
            });
        })
        .catch((err) => {
          console.log(err)
          res.status(400);
        });
    });
};
