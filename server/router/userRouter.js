import express from 'express';
import { checkLogon } from '../controller/userController.js';
import { jwtPassport } from '../utils/jwt-passport.js';
import { getUserByPID, createUser, updateUser, deleteUser, addGarden } from "../controller/userController.js"

let userRouter = express.Router();
let auth = jwtPassport();

userRouter.use(auth.initialize());
userRouter.post("/login", checkLogon);
userRouter.get("/logout", function (req, res) {
    req.logout();
    res.send({ done: "Logout successfully!" });
});


userRouter.get("/:pid", auth.authenticate(), getUserByPID)
userRouter.post('/addGarden', auth.authenticate(), addGarden)
userRouter.post("/createUser", auth.authenticate(), createUser)
userRouter.put("/update", auth.authenticate(), updateUser)
userRouter.delete("/remove/:id", auth.authenticate(), deleteUser)

export default userRouter;