import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;

const UserSchema = new mongoose.Schema({
  name: { type: String, trim: true, required: [true, "Name is required"] },
  surname: { type: String, trim: true, required: [true, "surnName is required"] },
  pid: { type: String, trim: true, unique: true, required: true },
  password: { type: String, required: true },
  address: { type: String, required: true },
  signUpDate: { type: Date, default: Date.now() },
  garden: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Garden' }],
  email: { type: String, trim: true, unique: true, required: true },
  role: { type: String, default: "user" }
});
export default mongoose.model("User", UserSchema, "User");
