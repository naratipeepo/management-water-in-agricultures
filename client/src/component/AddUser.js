import React from "react";
import history from '../utils/authUtils/history';
import axios from "axios";

export default function AddUser() {

  let data = {
    name: "",
    surname: "",
    pid: "",
    password: "",
    address: "",
    email: "",
  };

  function back() {
    history.replace("/Menu");
  }
  return (
    <div>
      <div className="addUserBox">
        <div className="dataUserBox">
          <h2>ข้อมูลเกษตรกร</h2>

          <div className="dataBox">
            <label htmlFor="uid">อีเมลล์ : </label>
            <input
              className="defInput"
              type="text"
              id="uid"
              name="uid"
              onChange={(e) => (data.email = e.target.value)}
            />

            <label htmlFor="password">รหัสผ่าน : </label>
            <input
              className="defInput"
              type="text"
              id="password"
              name="password"
              onChange={(e) => (data.password = e.target.value)}
            />

            <label htmlFor="name">ชื่อ : </label>
            <input
              className="defInput"
              type="text"
              id="name"
              name="name"
              onChange={(e) => (data.name = e.target.value)}
            />

            <label htmlFor="surname">นามสกุล : </label>
            <input
              className="defInput"
              type="text"
              id="surname"
              name="surname"
              onChange={(e) => (data.surname = e.target.value)}
            />

            <label id="pid" htmlFor="pid">
              เลขบัตรประจำตัวประชาชน :{" "}
            </label>
            <input
              className="defInput"
              type="text"
              id="pid"
              name="pid"
              onChange={(e) => (data.pid = e.target.value)}
            />

            <label htmlFor="address">ที่อยู่ : </label>
            <textarea
              className="defInput"
              type="text"
              id="address"
              name="address"
              onChange={(e) => (data.address = e.target.value)}
            />
          </div>
        </div>

        <div className="userbuttonBox">
          <button
            className="size1"
            id="add"
            onClick={() => {
              console.log(data);
              axios
                .post("/api/user/createUser", data ,{
                  headers: { 'Authorization':  localStorage.getItem("token")},
                })
                .then((r) => {
                  console.log(r);
                  history.push("/addSuccess")
                })
                .catch((e) => console.log(e));
            }}
          >
            เพิ่มข้อมูลเกษตรกร
          </button>
          <button className="size1" id="cancle" onClick={back}>กลับสู่เมนูแอดมิน</button>
        </div>
      </div>
    </div>
  );
}
