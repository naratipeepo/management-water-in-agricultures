// jwt-passport.js
import passport from "passport";
import passportJWT from "passport-jwt";
const JwtStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;
import User from "../model/userModel.js";
import { jwtSecret } from "../config/jwtConfig.js";

export const jwtPassport = () => {
  let params = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("bearer"),
    secretOrKey: jwtSecret, // 'very-secret'
  };
  var strategy = new JwtStrategy(params, (jwt_payload, done) => {
    User.findOne({ 'email': jwt_payload.email })
      .then((user) => {
        if (user) {
          return done(null, user);
        }
        return done(null, false);
      })
      .catch((err) => {
        return done(err, false);
      });
  });
  passport.use(strategy); // make passport use a specified strategy
  return {
    initialize: () => {
      console.log("initialize!!!");
      return passport.initialize();
    },
    authenticate: () => {
      console.log("auth: ");
      return passport.authenticate("jwt", { session: false }); // disable passport session
    },
  };
};
