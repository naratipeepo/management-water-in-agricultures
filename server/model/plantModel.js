import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;
var plantSchema = mongoose.Schema({
  name: String,
  age: String,
  plantInGarden: String,
  productInGarden: String,
  KP: Number,
  waterPerDay: Number,
});
let Plant = mongoose.model("Plant", plantSchema, "Plant");
export default Plant;