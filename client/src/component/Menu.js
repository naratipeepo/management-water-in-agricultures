import React from "react";
import "./Menu.css";
import { Link } from "react-router-dom";
import history from '../utils/authUtils/history';

export default function Menu() {

  function toExit(){
    localStorage.clear()
    history.push("/")
  }

  return (
      <span className="buttonBox">
        <Link  to="/AddUser" className="decoratedButton size1">
          <img src={require('../image/user.png')} alt="" width="85" height="90" />

          <h5>เพิ่มเกษตรเข้าสู่ระบบ</h5>
        </Link>

        <Link to="/AddGarden" className="decoratedButton size1" >
          <img
            src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/bf9d41232a30a4fb1c7fb8cf28a797a9.png"
            alt="createGarden"
            width="85"
            height="85"
          />
          <h5>สร้างแปลงผักให้กับเกษตรกร</h5>
        </Link>

        <Link to="/EditUser" className="decoratedButton size1">
          <img
            src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/cf6894cdabf521a998da3b7ac3339dd6.png"
            alt="edit"
            width="90"
            height="90"
          />
          <h5>แก้ไขข้อมูลเกษตรกร</h5>
        </Link>
        
        <span className="decoratedButton size1" onClick={toExit}> 
            <img
              src="https://image.flaticon.com/icons/svg/2618/2618662.svg"
              alt="edit"
              width="90"
              height="100"
            />
            <h5>ออกจากระบบ</h5>
          </span>

      </span>
  );
}
