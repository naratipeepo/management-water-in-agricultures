import React from "react";
import "./App.css";
import { Router, Switch } from "react-router-dom";
import history from "./utils/authUtils/history"

import Auth from "./utils/authUtils/auth";


import NavBar from "./component/NavBar";
import Context from "./utils/authUtils/Context";
import Routes from "./Routes";

function App() {
  const auth = new Auth();

  return (
    <Context.Provider
      value={{
        authObj: auth,
      }}
    >
      <Router id="app" history={history}>
        <NavBar />
        <div id="display">
          <Switch>
            <Routes />
          </Switch>
        </div>
      </Router>
    </Context.Provider>
  );
}

export default App;
