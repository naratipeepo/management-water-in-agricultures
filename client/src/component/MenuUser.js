import React from 'react'
import history from '../utils/authUtils/history';
import "./Menu.css";

export default function MenuUser() {

  function toFarmviewer() {
    history.push("/Farmviewer")
  }

  function toTablePlant() {
    history.push("/TablePlant")
  }

  function toExit() {
    localStorage.clear()
    history.push("/")
  }

  return (
    <div className="buttonBox">
      <span className="decoratedButton size1" onClick={toFarmviewer}>
        <img src={require('../image/farm.png')} alt="" width="90" height="100" />

        <h5>ฟาร์มของฉัน</h5>
      </span>

      <span className="decoratedButton size1" onClick={toTablePlant}>
        <img
          src="https://image.flaticon.com/icons/png/512/706/706195.png"
          alt="edit"
          width="95"
          height="100"
        />
        <h5>ข้อมูลผักผลไม้</h5>
      </span>

      <span className="decoratedButton size1" onClick={toExit}>
        <img
          src="https://image.flaticon.com/icons/svg/2618/2618662.svg"
          alt="edit"
          width="90"
          height="100"
        />
        <h5>ออกจากระบบ</h5>
      </span>
    </div>
  );
}
