import Garden from "../model/gardenModel.js"
import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;
var ObjectId = mongoose.Types.ObjectId;

export const createGarden = (req, res) => {
    res.send(200)
}

export const deleteGarden = (req, res) => {
    Garden.findByIdAndRemove({ "_id": new ObjectId(req.params.id) })
        .then(garden => {
            if (!garden) {
                res.status(404);
            }
            res.json({ success: true, message: "User deleted successfully!" });
        })
        .catch(err => res.status(404))
}