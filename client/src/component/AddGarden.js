import React, { useState, useEffect } from 'react'
import Axios from "axios";
import history from '../utils/authUtils/history';

export default function AddGarden() {
    let [count, setCount] = useState(0)

    let [location, setLocation] = useState([])

    let [points, setpoints] = useState([])

    const [pid, setPid] = useState("")

    let data = {
        pid: pid,
        color: "red",
        point: points,
        soilHumidity: 19,
        temperature: 39,
    };

    let handleLocate = () => {
        setpoints([...points, { "lat": 0, "lng": 0 }])

        setCount(++count)

        setLocation([
            ...location, count //destructure
        ])


    }

    function searchPID() {
        Axios
            .get("/api/user/" + pid,{
                headers: { 'Authorization':  localStorage.getItem("token")},
              })
            .then((e) => {
                console.log(e)
            })
    }

    function back() {
        history.push("/Menu")
    }

    const [plants, setplant] = useState([])
    //eslint-disable-next-line
    const [url, seturl] = useState("/api/plant/")

    useEffect(() => {
        async function fetchPlantData() {
            //await
            Axios.get(url,{
                headers: { 'Authorization':  localStorage.getItem("token")},
              })
                .then((res) => {
                    console.log(res)
                    setplant(res.data);
                    setCategory(res.data[0]._id)
                })
                .catch((err) => {
                    console.log(err)
                })
        }
        fetchPlantData();
    }, [url]);

    let [category, setCategory] = useState('');

    const handleCategoryChange = (category) => {
        setCategory(category);
        console.log(category);
    }

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
      }

    return (
        <div className="addGarden">
            <div className="HeadBox">
                เพิ่มแปลงผัก
            </div>

            <div className="boxInput">
                เลขบัตรประชาชนของเกษตรกร
                <img src={require('../image/user.png')} alt="" width="55" height="60" />
                <div>
                    <input
                        className="defInput"
                        type="text"
                        id="name"
                        name="name"
                        defaultValue={pid}
                        onChange={(e) => (setPid(e.target.value))}
                    />
                    <button id="searchBtn" onClick={searchPID} >
                        <span role="img" aria-label="search">🔎</span>
                    </button>
                </div>
            </div>

            <div className="boxInput">
                ชนิดพืชในแปลงผัก
                <div><span role="img" aria-label="vegetables">🥝 🍅 🍆 🥑 🥦 🥒 🥬 🌽 🥕</span></div>
                <select className="defInput" name="category" value={category} onChange={event => handleCategoryChange(event.target.value)}>
                    {
                        plants.map((plant, index) => {
                            return <option key={index} value={plant._id}>{plant.name}</option>
                        })
                    }
                </select>
            </div>

            <div className="boxInput">

                {
                    location.map((element, index) => {

                        return (<span className="center" key={index}>
                            จุดที่ {element}
                            <div className="dataBox" >
                                <label id="posX" htmlFor="posX">Lattitude :   </label>
                                <input
                                    className="defInput"
                                    type="text"
                                    id="posX"
                                    name="posX"
                                    onChange={(e) => {
                                        let tmp = points
                                        tmp[index].lat = Number(e.target.value)
                                        setpoints(tmp)
                                        console.log(points)
                                    }}
                                />

                                <label id="posY" htmlFor="posY">Longtitude : </label>
                                <input
                                    className="defInput"
                                    type="text"
                                    id="posY"
                                    name="posY"
                                    onChange={(e) => {
                                        let tmp = points
                                        tmp[index].lng = Number(e.target.value)
                                        setpoints(tmp)
                                        console.log(points)
                                    }}
                                />
                            </div>
                        </span>)
                    })
                }

                <div className="animateCursor" onClick={handleLocate}>
                    <img
                        src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/7a9a99f8f779ac8853345999e9bd82c3.png"
                        alt="logo"
                        width="80"
                        height="80"
                    />
                </div>
            </div>

            <div className="boxInput animateCursor" onClick={() => {

                data = {
                    color: "red",
                    point: points,
                    pid: pid,
                    soilHumidity: getRandomIntInclusive(1,99),
                    temperature: getRandomIntInclusive(20,45),
                    plant: [{ "_id": category }]
                };

                console.log(data)
                Axios
                    .post("/api/user/addGarden", data)
                    .then((e) => {
                        console.log(e)
                        history.push("/AddGardenSuccess")
                    })
                    .catch((e) => console.log(e))
            }}>
                สร้างแปลงผัก
            </div>
            <div className="HeadBox animateCursor" onClick={back}>
                กลับสู่เมนูแอดมิน
            </div>

        </div>
    )
}
