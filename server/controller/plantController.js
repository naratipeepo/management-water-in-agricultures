import Plant from "../model/plantModel.js"
import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;
var ObjectId = mongoose.Types.ObjectId;

export const getAllPlant = (req, res) => {
    Plant.find({})
        .then(plants => {
            res.json(plants);
        })
        .catch(err => {
            res.status(500);
        });
}

export const createPlant = (req, res) => {
    let { name, age, plantInGarden, productInGarden, KP, waterPerDay } = req.body
    var newPlant = new Plant({
        'name': name,
        'age': age,
        'plantInGarden': plantInGarden,
        'productInGarden': productInGarden,
        'KP': KP,
        'waterPerDay': waterPerDay
    })
    Plant.init()// avoid dup by wait until finish building index
        .then(function () {
            newPlant.save()
                .then(user => {
                    res.json({ success: true, message: 'Plant createPlant' });
                }).catch(err => {
                    res.status(400);
                });
        });
}

export const deletePlant = (req, res) => {
    Plant.findByIdAndRemove({ "_id": new ObjectId(req.params.id) })
        .then(plant => {
            if (!plant) {
                res.status(404).json(logError("User not found with id " + req.params.userId));
            }
            res.json({ success: true, message: "User deleted successfully!" });
        })
        .catch(err => res.status(404).json(logError(err)))
}

