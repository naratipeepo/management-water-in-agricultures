import express from 'express';
import { jwtPassport } from '../utils/jwt-passport.js';
import { getAllPlant, createPlant, deletePlant } from "../controller/plantController.js"

let plantRouter = express.Router();
let auth = jwtPassport();

plantRouter.use(auth.initialize());

plantRouter.get('/', auth.authenticate(), getAllPlant)
plantRouter.post('/add',auth.authenticate(),createPlant)
// plantRouter.post('/add', createPlant)
plantRouter.delete('/remove/:id', auth.authenticate(), deletePlant)

export default plantRouter 