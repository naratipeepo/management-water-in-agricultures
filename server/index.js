import express from "express";
import bodyParser from "body-parser";
import logger from "morgan";

import userRouter from "./router/userRouter.js";
import gardenRouter from "./router/gardenRouter.js";
import plantRouter from "./router/plantRouter.js";

var app = express();

app.use(express.static("public"));
app.use(logger("short"));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

import mongooseDbConnect from './config/database.js';
app.use(mongooseDbConnect())

app.use("/api/user", userRouter);
app.use("/api/garden", gardenRouter);
app.use("/api/plant", plantRouter);

app.get("/", (req, res) => res.send({ error: "Invalid Endport" }));
app.get("*", (req, res) => {
  throw new Error("Not Found Page!" + req.url);
});

var port = 4000;
app.listen(port, function () {
  console.log(`start http server on ${port}`);
});
