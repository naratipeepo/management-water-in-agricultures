import React, { useEffect } from "react";
import "./Success.css";
import history from '../utils/authUtils/history';

function Success(props) {

  useEffect(() => {
    async function timer() {

      setTimeout(() => {
        history.push("/Menu")
      }, 2000);

    }
    timer();
  }, []);

  return (
    <div id="success">
      <img src={require('../image/check2.png')} alt="" />
      <h1>{props.text}</h1>
    </div>
  );
}
export default Success;
