import express from 'express';
import { jwtPassport } from '../utils/jwt-passport.js';
import { createGarden, deleteGarden } from "../controller/gardenController.js"
let auth = jwtPassport();
let gardenRouter = express.Router();

gardenRouter.use(auth.initialize());

gardenRouter.post('/createGarden', auth.authenticate(), createGarden)
gardenRouter.delete('/remove/:id', auth.authenticate(), deleteGarden)

export default gardenRouter