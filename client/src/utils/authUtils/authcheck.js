import React, { useEffect, useContext } from "react";
import history from "./history";
import Context from "./Context";

const AuthCheck = (props) => {
  const context = useContext(Context);
  useEffect(() => {
    if (context.authObj.isAuthenticated()) {

      let isAdmin = context.authObj.getToken("role") === "admin";
      if (isAdmin)
        history.replace("/Menu");
      else
        history.replace("/MenuUser");
    } else {

      console.log("authcheck");

      setTimeout(() => {
        history.push("/");
      }, 600);
      //      history.replace('/')
    }
  }, [context]);

  // Redirect to another page; don't need to show anything
  return <></>;
};

export default AuthCheck;
