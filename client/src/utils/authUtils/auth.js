import history from './history';

class Auth {
  url = "/api/user/";
  profile = {};

  signin = (data, error) => { // data consists of email and password
    if (!data) throw error({ globalError: "no data for submission" })
    let option = {
      method: 'POST', headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    }
    fetch(`${this.url}login`, option)
      .then(result => {
        if (result.ok) return result.json()
        else throw result;
      })
      .then(result => {
        if (result) {
          let user = {
            token: result.token,
            pid: result.user.pid,
            role: result.user.role,
            expiresIn: result.user.expiresIn
          };
          localStorage.setItem('token', user.token);
          localStorage.setItem('pid', user.pid);
          localStorage.setItem('role', user.role);
          let expiresAt = JSON.stringify((user.expiresIn * 1000 + new Date().getTime()))
          localStorage.setItem('expiresAt', expiresAt)
          this.profile = user
          setTimeout(() => { history.replace('/authcheck') }, 600);
        }
      })
      .catch(async err => {
        alert("error")
      })
  };

  getToken = (token) => localStorage.getItem(token) ? localStorage.getItem(token) : null;

  signout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('pid')
    localStorage.removeItem('expiresAt')
    setTimeout(() => { history.push('/authcheck') }, 600);
  };

  isAuthenticated = () => {
    let expiresAt = JSON.parse(this.getToken('expiresAt'))
    return expiresAt && (new Date().getTime() < expiresAt)
  }
}

export default Auth;