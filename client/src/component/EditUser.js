import React, { useState } from "react";
import history from "../utils/authUtils/history";
import Axios from "axios";

export default function EditUser() {
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [pid, setPid] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [id, setID] = useState("");
  const [garden, setGarden] = useState([]);

  function searchPID() {
    Axios.get("/api/user/" + pid,{
      headers: { 'Authorization':  localStorage.getItem("token")},
    }).then((e) => {
        console.log(e)
      setName(e.data.name);
      setSurname(e.data.surname);
      setAddress(e.data.address);
      setEmail(e.data.email);
      setID(e.data._id);
      setGarden(e.data.garden);
    });
  }

  function removeID() {
    Axios.delete("/api/user/remove/" + id,{
      headers: { 'Authorization':  localStorage.getItem("token")},
    }).then((e) => {
      history.push("/RemoveAccountSuccess");
    });
  }

  function cancle() {
    history.push("/Menu");
  }

  function removePlant(index, id) {
    Axios.delete("/api/garden/remove/" + id,{
      headers: { 'Authorization':  localStorage.getItem("token")},
    })
      .then((e) => {
        const dupGarden = Object.assign([], garden);
        dupGarden.splice(index, 1);
        setGarden(dupGarden);
        alert("ลบแปลงผักสำเร็จ");
      })
      .catch((err) => {
        console.log(err);
      });
  } //BUG can not remove garden in user table

  return (
    <div>
      <div className="addUserBox">
        <div className="dataUserBox">
          <h2>ข้อมูลเกษตรกร</h2>

          <div className="dataBox">
            <label htmlFor="pid">เลขบัตรประจำตัวประชาชน : </label>
            <div>
              <input
                className="defInput"
                type="text"
                id="pid"
                name="pid"
                defaultValue={pid}
                onChange={(e) => setPid(e.target.value)}
              />
              <button id="searchBtn" onClick={searchPID}>
                <span role="img" aria-label="search">
                  🔎
                </span>
              </button>
            </div>

            <label htmlFor="uid">อีเมลล์ : </label>
            <input
              className="defInput"
              type="text"
              id="uid"
              name="uid"
              defaultValue={email}
              onChange={(e) => setEmail(e.target.value)}
            />

            <label htmlFor="password">รหัสผ่าน : </label>
            <input
              className="defInput"
              type="text"
              id="password"
              name="password"
              defaultValue={password}
              onChange={(e) => setPassword(e.target.value)}
            />

            <label htmlFor="name">ชื่อ : </label>
            <input
              className="defInput"
              type="text"
              id="name"
              name="name"
              defaultValue={name}
              onChange={(e) => setName(e.target.value)}
            />

            <label htmlFor="surname">นามสกุล : </label>
            <input
              className="defInput"
              type="text"
              id="surname"
              name="surname"
              defaultValue={surname}
              onChange={(e) => setSurname(e.target.value)}
            />

            <label htmlFor="address">ที่อยู่ : </label>
            <textarea
              className="defInput"
              type="text"
              id="address"
              name="address"
              defaultValue={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <br />
          <label>ข้อมูลแปลงผัก : </label>

          <div id="listPlant">
            {garden.map((e, index) => {
              return (
                <div className="itemsPlant"key={index}>
                  <input
                    className="itemShown"
                    type="text"
                    name="item1"
                    value={e.plants[0].name}
                    disabled
                  />

                  <img
                    className="animateCursor"
                    src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/50841e3c83e40e8234b462da95631976.png"
                    width="40"
                    height="40"
                    alt="1"
                    key={index}
                    onClick={() => removePlant(index, e._id)} //can pass parameter each unique object
                  />
                </div>
              );
            })}
          </div>
        </div>

        <div className="userbuttonBox">
          <button
            className="size1"
            id="add"
            onClick={() => {
              var data;

              if (password.length === 0) {
                data = {
                  name: name,
                  surname: surname,
                  pid: pid,
                  address: address,
                  email: email,
                };
                console.log(password);
              }
              if (password.length > 0) {
                data = {
                  name: name,
                  surname: surname,
                  pid: pid,
                  password: password,
                  address: address,
                  email: email,
                };
              }

              Axios.put("/api/user/update", data,{
                headers: { 'Authorization':  localStorage.getItem("token")},
              })
                .then((r) => {
                  console.log(r);
                  history.push("/EditSuccess");
                })
                .catch((e) => console.log(e));
            }}
          >
            อัพเดทข้อมูลเกษตรกร
          </button>
          <button className="size1" id="cancle" onClick={cancle}>
            กลับสู่เมนูแอดมิน
          </button>
          <button className="size1" id="delete" onClick={removeID}>
            ลบแอคเค้าท์
          </button>
        </div>
      </div>
    </div>
  );
}
