import React, { useContext } from "react";
import Context from '../utils/authUtils/Context'

//css in app.css
export default function Login(props) {
  const context = useContext(Context);
  var data = {
    email: "",
    password: "",
  };

  function handleLogin() {
    context.authObj.signin(data)
  }
  return (
    <div className="LoginBox">
      <img
        className="imgBox"
        src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/17722e04161cf8ba5c3f024e84b899a7.png"
        alt="logo"
        height="300"
      />

      <form className="formBox">
        <div>
          <input
            className="defInput"
            type="text"
            id="uid"
            name="uid"
            placeholder=" email"
            onChange={(e) => (data.email = e.target.value)}
          />
          <br />
          <input
            className="defInput"
            type="password"
            id="password"
            name="password"
            placeholder=" password"
            onChange={(e) => (data.password = e.target.value)}
          />
        </div>

        <img
          id="signIn"
          className="animateCursor"
          src="https://s3-ap-southeast-1.amazonaws.com/img-in-th/63fcdc7e79e223deb4fc1fd315ec3926.png"
          alt="logo"
          width="80"
          height="80"
          onClick={handleLogin}
        />
      </form>
    </div>
  );
}
