import * as mongooseDef from "mongoose";
let mongoose = mongooseDef.default;
var gardenSchema = mongoose.Schema({
  color: String,
  point: [{}],
  soilHumidity: Number,
  temperature: Number,
  plants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Plant' }],
});
let Garden = mongoose.model("Garden", gardenSchema, "Garden");
export default Garden;