import React from "react";
import { Route } from "react-router-dom";
import Login from "./component/Login";
import Menu from "./component/Menu";
import Success from "./component/Success";
import AddGarden from "./component/AddGarden";
import AddUser from "./component/AddUser";
import EditUser from "./component/EditUser";
import FarmViewer from "./component/FarmViewer";
import MenuUser from "./component/MenuUser";
import TablePlant from "./component/TablePlant"
import PrivateRoute from "./utils/PrivateRoute";
import AuthCheck from "./utils/authUtils/authcheck";

export default function Routes() {
  return (
    <>
      <Route path="/" exact component={Login} />
      <Route
        path="/Login"
        render={(setState) => <Login state={setState}></Login>}
      />

      <Route path="/authcheck" exact component={AuthCheck} />
      <PrivateRoute
        path="/Menu"
        component={Menu}
        requiredAdmin={true}
        redirectTo="/Login"
        fromPage="/Menu"
      />
      <PrivateRoute
        path="/AddGarden"
        component={AddGarden}
        requiredAdmin={true}
        redirectTo="/Login"
        fromPage="/AddGarden"
      />
      <PrivateRoute
        path="/AddUser"
        component={AddUser}
        requiredAdmin={true}
        redirectTo="/Login"
        fromPage="/AddUser"
      />
      <PrivateRoute
        path="/EditUser"
        component={EditUser}
        requiredAdmin={true}
        redirectTo="/Login"
        fromPage="/EditUser"
      />
      <PrivateRoute
        path="/Farmviewer"
         component={FarmViewer}
        redirectTo="/Login"
        fromPage="/Farmviewer"
       />
      <PrivateRoute
        path="/Farmviewer"
        component={FarmViewer}
        redirectTo="/Login"
        fromPage="/MenuUser"
      />
      <PrivateRoute
        path="/MenuUser"
        component={MenuUser}
        redirectTo="/Login"
        fromPage="/MenuUser"
      />
      <PrivateRoute
        path="/TablePlant"
        component={TablePlant}
        redirectTo="/Login"
        fromPage="/MenuUser"
      />
     
      <Route path="/AddGardenSuccess">
        <Success text="สร้างแปลงผักเรียบร้อย" />
      </Route>
      <Route path="/addSuccess">
        <Success text="เพิ่มเกษตรกรเรียบร้อยแล้ว" />
      </Route>
      <Route path="/EditSuccess">
        <Success text="แก้ไขข้อมูลเรียบร้อย" />
      </Route>
      <Route path="/RemoveAccountSuccess">
        <Success text="ทำการลบแอคเค้าท์ของคุณแล้ว" />
      </Route>
    </>
  );
}
