import React, { Component } from "react";
import { Map, GoogleApiWrapper, Polygon, Marker, InfoWindow } from "google-maps-react";
import { MAP_API_KEY } from '../config/env'
import Axios from 'axios'

let tmpLat;
let tmpLng;

export class MapContainer extends Component {

  state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    garden: [{ point: [{ lat: -21.805050, lng: -49.094298 }] }]
  };

  UNSAFE_componentWillMount() {
    Axios
      .get('/api/user/' + localStorage.getItem("pid"),{
        headers: { 'Authorization':  localStorage.getItem("token")},
      })
      .then((e) => {
        // console.log(e.data.garden[0].point)
        let tmp = this.state
        tmp.garden = e.data.garden
        this.setState(tmp)
      })

  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  averagePoint = (point) => {
    tmpLat = 0;
    tmpLng = 0;
    let count = 0;
   

    for (let y of point) {
      tmpLat += y.lat;
      tmpLng += y.lng;
      count++;
    }

    tmpLat = tmpLat / count;
    tmpLng = tmpLng / count;

    console.log("lat: "+ tmpLat +" lng: "+tmpLng)

     return {"lat":tmpLat,"lng":tmpLng}

  }

  waterCalculator(currentlyWater,waterNeed){
    if((waterNeed*10) <= currentlyWater){
      return "ต้องรดน้ำแล้วนะครับ"
    }else{
      return "ยังไม่ต้องรดน้ำ"
    }
  }

  waterCalculatorEmoji(currentlyWater,waterNeed){
    if((waterNeed*10) <= currentlyWater){
      return "🥵"
    }else{
      return "😇"
    }
  }

  render() {

    // this.averagePoint(point)

    return (

      <Map
        containerStyle={{ position: "fixed", height: "100%", width: "100%", padding: "0px", margin: 0, left: "0.5%", top: "11%" }}
        initialCenter={{"lat":13.756331,"lng":100.501762}}
        google={this.props.google}
        style={{ height: '90%', width: '98.9%' }}
        zoom={6}
        scrollwheel={false}
      >

        {this.state.garden.map((e, index) => {
          return (
            <Polygon
              key={index}
              paths={e.point}
              onMouseover={() => console.log('mouseover')}
              onClick={() => console.log("onclick")}
              onMouseout={() => console.log('mouseout')}
              strokeColor='transparent'
              strokeOpacity={0}
              strokeWeight={5}
              fillColor='#FF0000'
              fillOpacity={0.2}
            />
          )
        })}

        {this.state.garden.map((e, index) => {
        
          if(e.plants){
          return <Marker onClick={this.onMarkerClick}
            key={index}
            position={this.averagePoint(e.point)}
            name={`${this.waterCalculatorEmoji(e.soilHumidity,e.plants[0].waterPerDay)} : ต้น${e.plants[0].name}${this.waterCalculator(e.soilHumidity,e.plants[0].waterPerDay)}\n`}
            icon={{
              url: "https://www.img.in.th/images/f01438693ee0bfb57078313dd3179b65.png",
            }}
          />}else{
            return ""
          }
        })}


        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}>
          <div>
            <h1>{this.state.selectedPlace.name}</h1>
          </div>
        </InfoWindow>

      </Map>

    );
  }
}

export default GoogleApiWrapper({
  apiKey: MAP_API_KEY,
})(MapContainer);
