import React, { useEffect, useState } from 'react'
import history from '../utils/authUtils/history';
import Axios from "axios";

export default function TablePlant() {

    const [plants, setplant] = useState([])
    //eslint-disable-next-line
    const [url, seturl] = useState("/api/plant/")

    useEffect(() => {
        async function fetchPlantData() {
            //await
            Axios.get(url,{
                headers: { 'Authorization':  localStorage.getItem("token")},
              })
                .then((res) => {
                    setplant(res.data);
                })
                .catch((err) => {
                    console.log(err)
                })
        }
        fetchPlantData();
    }, [url]);

    function back() {
        history.push("/MenuUser")
    }

    return (
        <div className="tablePlant">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>พืช</th>
                        <th>อายุเก็บเกี่ยว (วัน)</th>
                        <th>จำนวนต้น/แปลง (ซม.)</th>
                        <th>ได้ผลผลิต (ตัน/แปลง)</th>
                        <th>ค่า KP</th>
                        <th>ปริมาณน้ำต่อวัน (ลบ.ม/ต้น)</th>
                    </tr>
                </thead>

                <tbody>
                    {plants.map((e, index) => {

                        let header = Object.values(e)
                            .map((e, index) => <td key={index}>{e}</td>);

                        return <tr key={index}>{header}</tr>
                    })}
                </tbody>
            </table>
            <button className="size1" id="back" onClick={back}>ย้อนกลับ</button>

        </div>
    )
}
